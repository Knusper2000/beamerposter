\documentclass{beamer}

\usepackage[orientation=portrait,size=a0,scale=1.4]{beamerposter}
\mode<presentation>{\usetheme{ULeiden}}
\usepackage[utf8]{inputenc}
\usepackage{ragged2e}
\usepackage[font=small,justification=justified]{caption}
\usepackage{array,booktabs,tabularx}
\usepackage{grffile}

% \usefonttheme{professionalfonts}
%\usepackage{sfmath}

\DeclareRobustCommand{\ion}[2]{\textup{#1\,\textsc{\textup{#2}}}}
\newcommand*\arcmin{\ensuremath{^\prime}}
\newcommand*\arcsec{\ensuremath{^{\prime\prime}}}
\DeclareMathOperator\erf{erf}

\title[MXDF LAE LF]{Towards an unbiased census of high-$z$
  Lyman-$\alpha$ emitters \\ in the MUSE Ultra Deep Field Surveys}

\author{Edmund~Christian~Herenz$^{1,2}$ and the MUSE GTO Team}

\institute[]{$^1$ESO Fellow, Santiago, Chile\; $^2$Leiden Observatory,
  Leiden, The Netherlands}

\date{Escape of Lyman radiation from galactic labyrinths (Crete, April
  18-21, 2023)}

\begin{document}

\begin{frame}
  \begin{columns}[t]
    \begin{column}{0.48\textwidth}
      \begin{block}{Motivation: The faint-end of the $z>6$ Ly$\alpha$
          Luminosity Function}
        The observable Ly$\alpha$ emission from galaxies during the Epoch of Reionisation depends
        sensitively on the residual volume averaged neutral fraction, $x_\mathsf{HI}$, within the
        Universe.  An observed overall decline of the Ly$\alpha$ emitter luminosity function (LAE
        LF) from $z \approx 6$ to $z \approx 7$ is linked to a rapidly decreasing
        $x_\mathsf{HI} \rightarrow 0$ from $0.5$ over $\lesssim 200$\,Myr [1,2].  Consistent with
        other probes, we deduce that reionisation was essentially complete (i.e.
        $x_\mathsf{HI} \approx 0$) at $z \approx 6$ [3].  Nevertheless, inferring $x_\mathsf{HI}$
        solely from the integrated LAE LF is highly model dependent and current LAE samples at those
        epochs are limited to $\log L_\mathsf{Ly\alpha} [\mathsf{erg\,s^{-1}}] \gtrsim 42.3$.  As of
        yet, we do not have strong constraints on the sub-$L_\star$ ``faint-end'' of the LAE LF.
        Interestingly, especially at sub-$L_\star$ the LAE LF is expected to show the strongest
        attenuation with increasing $z$, if reionisation proceeds ``bottom-up''~[4,5].  The deepest
        blind MUSE surveys [6-8], such as the MUSE GTO Hubble Ultra Deep Field Surveys [7,8], that
        encompasses the 141\,h deep MUSE eXtremely Deep Field [8] (MXDF), finally allow for
        constraints in this respect. \vspace{0.3cm}
      \end{block}
      \vspace{0.1cm}
      \begin{block}{A robust $z>6$ LAE MXDF sample with LSDCat2.0}
        \begin{figure}
          \centering \includegraphics[width=0.9\textwidth,trim=10 15 30
          20,clip=true]{figures/plot_source_expmap.pdf}
          \caption{Positions of $z>6$ LAEs in the MXDF, colour coded by their LSDCat2.0
            matched-filter SN, plotted over the $t_\mathsf{exp}$ map.  Circles correspond sources
            within the reference LSDCat2.0 catalogue ($\mathsf{SN}_\mathsf{thresh} > 5.4$), whereas
            sources only in the DR2 catalogue are shown by squares.}
          \label{fig:1}
        \end{figure}
        An unbiased census of the high-$z$ galaxy population within the deepest MUSE fields requires
        a pure sample not contaminated by spurious detections and foreground galaxies with a
        well understood selection function.  But, unfortunately the selection function of the
        published deep-field catalogues is unknown.  Constructing a LAE sample with LSDCat2.0 [9,10]
        removes this problem, as the selection function of its matched filter is deterministic (see
        next box).  To illustrate the difference between the catalogues, we compare in
        Figures~\ref{fig:1}~\&~\ref{fig:2} our LSDCat2.0 $z>6$ sample to the published catalogue in
        the MXDF [8].  The published catalogue contains 15 $z>6$ LAEs within the MXDF, 8 of which
        are recovered by LSDCat. The remaining 7 are at a lower SN, where the number of spurious
        detections would require significant manual cleaning of the catalogue.
        \begin{figure}
          \centering
          \includegraphics[width=0.8\textwidth]{figures/plot_flux_vs_lambda.pdf}
          \caption{Line flux vs. detection wavelength of LAEs at $z>6$ in the MXDF.  Blue (open)
            squares show sources from DR2 (outside the deep $t_\mathsf{exp} > 135$\,h region),
            whereas LSDCat2.0 sources and measurements are shown with green circles.  The grey
            line shows the 50\% completeness limit of the idealised LSDCat2.0 selection function at
            $\text{SN}_\mathsf{thresh} > 5.4$ (cf. Fig.~\ref{fig:3}).}
          \label{fig:2}
        \end{figure}
        (The LSDCat2.0 catalogue also contains lower $z$ galaxies not tabulated in [8].)
        \vspace{0.1cm}
      \end{block}
    \end{column}
    \begin{column}{0.48\textwidth}
      \begin{block}{The selection function for the \texttt{LSDCat2.0} sample}
        In ref. [10] it is derived that the selection function for catalogues of line emitters
        constructed with LSDCat2.0, $f_C$, can be written as
        \begin{equation}
          \label{eq:1}
          f_C(F_\mathsf{line},\lambda | \mathsf{SN}_\mathsf{thresh}) =
          \frac{1}{2} \left [ 1 + \erf \left ( \frac{C(\lambda) \cdot F_\mathsf{line} -
                \mathsf{SN}_\mathsf{thresh}}{\sqrt{2}} \right ) \right ] \;\text{,}
        \end{equation}
        where $F_\mathsf{line}$ is the emission line flux of a line at wavelength $\lambda$,
        $\mathsf{SN}_\mathsf{thresh}$ is the detection threshold, and
        $ \erf (x) = \frac{2}{\sqrt{\pi}}\int_{0}^{x}e^{-t^{2}}\,\mathsf{d}t = 1 -
        \frac{2}{\sqrt{\pi}} \int_x^\infty e^{-t^2} \, \mathsf{d} t$.  The scaling factor
        $C(\lambda)$ in Eq.~(\ref{eq:1}) depends solely on the used search template and the match
        between this template and the detected source.  For a perfect source-template match we have
        \begin{equation}
          \label{eq:2}
          C(\lambda [z]) = \frac{1}{\Delta\lambda}  \cdot 
          \sqrt{ \sum_{ij} \left ( S_{ij}^{\{\lambda[z] \}}
            \right )^2 } \cdot \sqrt{\sum_{k}  \frac{\left ( S_k^{\{\lambda[z] \}}  \right
              )^2}{\sigma_{z-k}^2} }  \;\text{.}
        \end{equation}
        where $S_{ij}^{\{\lambda[z]\}}$ and $S_k^{\{\lambda[z]\}}$ are the spatial and spectral
        shapes of the 3D matched filter, respectively, $\sigma_z^2$ is the effective variance at
        wavelength layer $\lambda[z]$, and $\Delta \lambda$ is the spectral width of a layer in the
        datacube.  Figure~\ref{fig:3} compares this idealised selection function for the used
        Gaussian 3D search template in the MXDF [10] and $\mathsf{SN}_\mathsf{thresh} = 5.4$ to a
        more realistic selection function, that models the expected Ly$\alpha$ halos of high-$z$
        LAEs following the profiles derived in [11].
        \begin{figure}
          \centering
          \includegraphics[width=\textwidth]{figures/plt_ideal_realistic.pdf}
          \caption{Comparison of the idealised selection function (\textit{left panel}) to a more
            realistic selection function (\textit{right panel}) that accounts for the expected
            variations in the low-SB halos of LAEs.}
          \label{fig:3}
        \end{figure}
      \end{block}
      \begin{block}{Faint $z>6$ LAE number counts - expectations vs. reality}
        That $\log L_\mathsf{Ly\alpha} [\mathsf{erg\,s^{-1}}] \lesssim 41.5$ LAEs at $z>6$ are an
        observationally unconstrained \textit{terra incognita} can be appreciated when comparing
        number counts,
        \begin{equation}
          \label{eq:3}
          N_\mathsf{LAE}(> F_\mathsf{Ly\alpha})  = 4 \pi
          \int_{F_\mathsf{Ly\alpha}}^\infty \mathsf{d}F \,
          \int_{z_\mathsf{min}}^{z_\mathsf{max}} \mathsf{d}z \,
          \left ( \frac{\mathsf{d}V}{\mathsf{d}z} \right)^\mathsf{\Lambda CDM}_{(z)} \phi \left ( L(F,z) \right )  D_L(z)^2 \;\text{,}
        \end{equation}
        predicted from extrapolated Schechter LAE LF parameterisations, $\phi(L)$, from the
        literature [1,2,12-15].  Less than ten to more than 100 LAEs are expected at the flux limits
        reached in the deepest MUSE surveys, depending on the reference.  We compare in
        Fig.~\ref{fig:4} these expectations with the completeness uncorrected number counts of the
        MXDF. It becomes clear, that the completeness correction will significantly affect the
        outcome of LAE LFs estimated from such samples.  Using the well defined $f_C$ of samples
        constructed with LSDCat2.0 now finally allows to tackle this problem in a sound manner.
        \begin{figure}
          \centering
          \includegraphics[width=0.9\textwidth]{figures/plot_cumnd_flux_ztgt6_mxdfdr2.pdf}
          \caption{Number count predictions, $N(>F)$, of faint $z\gtrsim6$ LAEs according to
            extrapolated Schechter parameterisations from the literature (refs. [1,2,12-15]) in
            comparison to completeness uncorrected number counts in the MXDF.}
          \label{fig:4}
        \end{figure}
      \end{block}
      % \begin{block}{Outlook}
      % \end{block}%\vspace{-0.cm}
    \end{column}
  \end{columns}
  \begin{columns}
    \begin{column}{0.99\textwidth} \vspace{-0.6cm}
      \begin{block}{\small References}
        \footnotesize
        [1] Wold, N., et al. 2022, ApJ 972, 36. \,
        [2] Konno, A., et al. 2018, PASJ 70, S16. \,
        [3] Finkelstein, S. 2016, PASA 33, e037. \,
        [4] Haiman, Z. \& Cen, R., ApJ 623, 627. \,
        [5] Matthee, J., et al. 2015, MNRAS 451, 400. \,
        [6] Lusso, E., et al. 2019, MNRAS 485, L62. \,
        [7] Bacon, R., et al. 2017, A\&A 608, A1. \,
        [8] Bacon, R., et al. 2023, A\&A 670, A4. \,
        [9] Herenz, E.C \& Wisotzki, L. 2017, A\&A 602, A111. \,
        [10] Herenz, E.C. 2023, AN (in press), e909. \,
        \mbox{[11] Wisotzki} et al. 2018, Nature 562, 229. \,
        [12] Herenz, E.C., et al. 2019, A\&A 621, A107. \,
        [13] de La Vieuville, G,. et al. 2019, A\&A 628, A3. \,
        [14] Konno, A., et al. 2018, PASJ 70, S16. 
        [15] Drake, A., et al. 2017, A\&A 608, A6. 
      \end{block}
    \end{column}
  \end{columns}
\end{frame}


\end{document}


%%% Local Variables:
%%% mode: latex
%%% TeX-master: t
%%% End:

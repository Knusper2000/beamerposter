# LaTeX Poster Templates (beamerposter) #

This repository contains posters that I made with LaTeX and the
[beamerposter](https://ctan.org/pkg/beamerposter) package.  The
`beamerposter` package is an build of the popular LaTeX package
`beamer`.  Its purpose is to enable the creation of beautiful looking
scientific posters.  The examples found here should demonstrate this.


## ESO ##

The style of these posters try to match the corporate identity of the
European Southern Observatory.

### Examples ###

The folder `eso` contains a poster presenting work regarding our
attempts to decipher the nature of the ionised filaments in a
starburst galaxy (see [Herenz et al. 2023; A&A 670,
A121](https://doi.org/10.1051/0004-6361/202244930 "A ∼15 kpc Outflow
Cone Piercing Through The Halo Of The Blue Compact Metal-poor Galaxy
SBS 0335-052E"))

![eso poster demo](./thumbs/sbs_poster2022_demo.png)

The folder `eso_kavli` contains the source for the poster that I
presented at the Joint Observatories Kavli Science Forum, Santiago
(Chile), 2022.  This poster is also made available online at Zenodo:
[![DOI](https://zenodo.org/badge/DOI/10.5281/zenodo.6471630.svg)](https://doi.org/10.5281/zenodo.6471630)

![lsdcat2 poster demo](./thumbs/lsdcat2_poster_demo.png)

See also [Herenz (2023; AN, in press)](https://doi.org/10.1002/asna.20220091 "Revisiting the Emission Line Source Detection Problem in Integral Field Spectroscopic Data").

The folder `eso_leiden` contains a poster that includes the logos from
two institutions (European Southern Observatory and Leiden University)
in the header.

![mxdf laelf poster demo](./thumbs/mxdf_laelf_poster.png)

## Stockholm University ##

The style of this poster tries to match the corporate identity of
Stockholm University.

In particular, the folder `stockholm_university` contains a poster presenting the
discovery of ionised filaments in the halo of the galaxy SBS
0335-052E.  This discovery, reported in in [Herenz et al. 2017, A&A
606, L11](https://doi.org/10.1051/0004-6361/201731809), was made
possible due to the unprecentedented capabilities of the MUSE
instrument at ESOs Very Large Telescope UT4. 

![su poster demo](./thumbs/sbs_poster_demo.png)


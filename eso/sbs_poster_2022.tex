\documentclass{beamer}

\usepackage[orientation=portrait,size=a0,scale=1.4]{beamerposter}
\mode<presentation>{\usetheme{ESO}}
\usepackage[utf8]{inputenc}
\usepackage{ragged2e}
\usepackage[font=small,justification=justified]{caption}
\usepackage{array,booktabs,tabularx}
\usepackage{grffile}

\DeclareRobustCommand{\ion}[2]{\textup{#1\,\textsc{\textup{#2}}}}
\newcommand*\arcmin{\ensuremath{^\prime}}
\newcommand*\arcsec{\ensuremath{^{\prime\prime}}}



\title[SBS\,0335-052E]{A $\sim$15 kpc outflow cone emanating from the compact starburst SBS\,0335-052E}

\author{E.~C.~Herenz$^1$, J.~M.~Cannon$^2$, J.~Inoue$^2$, M.~Hayes$^3$, C.~Moya-Sierralta$^4$, P.~Papaderos$^5$, and G.~Östlin$^3$}

\institute[]{$^1$ESO Fellow, Chile -- $^2$Macalester College, USA
  -- $^3$Stockholm University, Sweden -- $^4$Pontificia Universidad
  Cat\'{o}lica de Chile -- $^5$Institute of Astrophysics and Space
  Sciences, Portugal}

\date{LymanRAS Online Workshop -- Jan 14, 2022}

\begin{document}

\begin{frame}
  \begin{columns}[c]
    \begin{column}{0.99\textwidth} \vspace{+0.3em}
      \begin{block}{Spectacular outflow cone in high-z analogue may hint
          at anisotropic ionising photon leakage during the Epoch of
          Reionisation}
        \large The young ($\lesssim10$\,Myr) and metal-poor
        ($\text{Z}\sim1/50 \text{Z}_\odot$) starburst SBS 0335-052E
        ($D = 52$\,Mpc), commonly regarded as an early universe
        analogue, forms stars ($\sim$1\,M$_\odot$\,yr$^{-1}$) in a
        handful of super star clusters that are concentrated within
        500 pc.  MUSE observations reveal two bifurcated filamentary
        threads in H$\alpha$
        ($\text{SB}\sim10^{-18}$erg\,s$^{-1}$cm$^{-2}$arcsec$^{-2}$).
        They extend $10 - 15$\,kpc radially from the super star
        clusters and spread out symmetrically with respect to the
        kinematical minor axis.  JVLA B-configuration 21\,cm
        observations show HI extending into the filaments direction,
        but on larger scales the filaments are situated in a zone
        devoid of detectable HI.  We reason that the filaments are
        limb-brightened edges of a giant cone caused by outflowing gas
        protruding through the neutral halo of this gas-rich system.
        Such directional outflows in galaxies during the Epoch of
        Reionisation should lead to anisotropic ionising photon
        leakage.
        \end{block}
    \end{column}
  \end{columns}
  \vspace{0.5cm}
  \begin{columns}[T]
    \begin{column}{0.48\textwidth}
      \begin{beamercolorbox}[center,wd=\textwidth]{postercolumn}
        \begin{block}{MUSE \& JVLA observations - unprecedented
            view on HI and HII} 
          The galaxy pair SBS\,0335-052 E\&W is connected by a diffuse
          HI bridge that evidences the onset of merging.  On
          $\sim1$\,kpc scales the E galaxy shows a complex HII
          morphology, characterised by multiple arcs and arc-crossing
          filaments.  The former trace shells swept up by feedback
          from super star clusters and the latter indicate regions
          where the hot gas vents as the shells fragment.  We
          interpret the extended HII structure towards the NW as a
          large-scale outflow caused by the hot wind fluid penetrating
          through the porous circum-galactic medium.
          \vspace{0.5cm}
          \begin{figure}
            \begin{minipage}{\textwidth}
              \centering
              \includegraphics[width=\textwidth, trim=65 38 30 30,
              clip=true]{./figs2021/plot_rgb_muse_inset_N0_7ktaper.pdf}
              \\\vspace{1em}
              \includegraphics[width=0.475\textwidth, trim=90 23 31 5,
              clip=true]{./figs2021/plot_Ha_w_cont_21cm_notaper.pdf} 
              \includegraphics[width=0.45\textwidth,trim=80 25 60
              0,clip=true]{./figs2021/hst_f656n_21cm_newdat.pdf}
              \vspace{1em}            
              \caption{\emph{Top:} 30h JVLA B-configuration
                observations of SBS 0335-052 system (red contours at
                $N_\text{HI}= \{0.7, 1.6, 2.8, 3.6, 5.4 \} \times
                10^{20}$\,cm$^{-2}$; 7k$\lambda$ tapered data; beam is
                15.9\arcsec{}$\times$14.7\arcsec{} oriented at
                66.4$^\circ$) overlaid on H$\alpha$ narrow-band from
                MUSE (log-stretch from 0 to
                1.25$\times10^{-15}$\,erg\,s$^{-1}$cm$^{-2}$) inset
                into a colour composite (grz from
                Pan-STARRS). \emph{Bottom left:} H$\alpha$ from MUSE
                ($t_\text{exp.}=1.5$\,h, cyclic colour map from 0 to
                $10^{-18}$ to $10^{-15}$\,erg\,s$^{-1}$cm$^{-2}$ with
                asinh-scaling; contours
                \(\text{SB}_{H\alpha}=\{0.75, 1.5, 2.5, 5, 12.5\}
                \times 10^{-18}\)\,erg\,s$^{-1}$cm$^{-2}$) with
                untapered HI 21cm from JVLA. \emph{Bottom right:}
                Untapered JVLA 21cm B-configuration observations (red
                contours; 6.1\arcsec{}$\times$6.1\arcsec{} beam at
                -23.6$^\circ$).  Contour levels represent
                $N_\text{HI} = \{6, 9, 11, 14, 18.5, 27.8\}\times
                10^{20}\,\text{cm}^{-2}$.  The background HST
                H$\alpha$ (FR656N) image is displayed with a cyclic
                asinh-stretch highlighting the bright emission near
                the super star clusters while also providing contrast
                for the filamentary loops in the NW.  }
              \label{fig:1}
            \end{minipage}
          \end{figure}
          The untapered HI observations reveal extended HI towards the
          NW, i.e. towards the direction of the outflow.  Moreover,
          the HI peak is offset from the photometric centre; not an
          unusual sight in compact star-forming galaxies.
        \end{block}\vspace{-0.5cm}
      \end{beamercolorbox}
    \end{column}
    \begin{column}{0.48\textwidth}
      \begin{block}{HII kinematics - indication of conical outflow}
        The line-of-sight velocity field shows a NW-SE gradient that
        is, however, significantly perturbed in the centre.  This
        central perturbation is consistent with the expected signal
        from an expanding shell.  Notably, the imprint of this small
        scale feature stretches out continuously into the extended
        structure.
        \begin{figure}
          \centering \includegraphics[width=0.77\textwidth, trim=95 25
          0 0, clip=true]{./figs2021/plot_velfield_experiment.pdf} \vspace{1cm}
          \caption{Velocity field of the ionised gas as derived from
            the MUSE observations.  The major- and minor-axes from HI
            kinematics are delineated as dash-doted and dotted lines,
            respectively.}
          \label{fig:2}
        \end{figure}
      \end{block}
      \vspace{1.1cm}
      \begin{block}{A toy-model as aid to interpret the
          observations}
        A simple cone model (opening angle 19$^\circ$, tilt
        52$^\circ$, inclination 37$^\circ$) with HII
        ($T=2.5\times10^4$K, $n=0.1$cm$^{-3}$) walls of 1.8\,kpc
        thickness and 10\,kpc height can reproduce, at least to some
        extent, the extended H$\alpha$ morphology.  We emphasise, that
        the density is far lower than typical densities of HII gas
        ($100\times$ for even the lower end of HII regions), and we
        expect this gas is cooler material mixed in with the wind
        fluid.
%          \vspaceo{-0.3cm}
          \begin{figure}
            \centering \includegraphics[width=0.49\textwidth, trim=95 5
            0 20, clip=true]{./figs2021/cone_model_simple_1.pdf}
            \includegraphics[width=0.49\textwidth, trim=95 5 0 20,
            clip=true]{./figs2021/cone_model_simple_2.pdf}
            \caption{Conic model of hydrogen in ionisation equilibrium
              with parameters and at dimensions consistent with
              observations.  \emph{Left:} Display in physical
              coordinates at surface luminosities in cgs units.
              \emph{Right:} Simulated MUSE observations of the model
              structure at the distance of SBS\,0335-052 and with noise
              properties similar as in the real data. }
            \label{fig:model}
          \end{figure}
        \end{block}
    \end{column}
  \end{columns}
  \vspace{0.5cm}
  \begin{columns}
    \begin{column}{0.99\textwidth}
      \begin{block}{Conclusions}
        \large Our observations demonstrate that a compact
        low-metallicity starburst can drive a beamed outflow through
        an extended neutral halo.  Such outflows can act as channels
        for the leakage of ionising radiation.  Interestingly, the
        cone direction (NW) is opposite to the age gradient of star
        clusters (SE) Thus the directionality of Lyman contiuum photon
        escape could be intrinsically linked to the galaxy formation
        process.  The age gradient is interpreted as propagating star
        formation. If this is an important building mechanism in the
        formation of galaxies in the early universe, then we expected
        beamed ionising radiation leakage into small solid angles from
        numerous low-mass galaxies during the Epoch of Reionisation.
        Such a scenario should have effects on the topology of
        reionisation.
      \end{block}
    \end{column}
  \end{columns}
\end{frame}


\end{document}


%%% Local Variables:
%%% mode: latex
%%% TeX-master: t
%%% End:

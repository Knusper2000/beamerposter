\documentclass{beamer}
\usepackage[orientation=portrait,size=a0,scale=1.4,debug]{beamerposter}
%\mode<presentation>{\usetheme{Berlin}}
\mode<presentation>{\usetheme{SU}}
%\usepackage{chemformula}
\usepackage[utf8]{inputenc}
%\usepackage[german, english]{babel} % required for rendering German special characters
\usepackage{siunitx} %pretty measurement unit rendering
%\usepackage{hyperref} %enable hyperlink for urls
\usepackage{ragged2e}
\usepackage[font=small,justification=justified]{caption}
\usepackage{array,booktabs,tabularx}
\usepackage{grffile}

\DeclareRobustCommand{\ion}[2]{\textup{#1\,\textsc{\textup{#2}}}}
\newcommand*\arcmin{\ensuremath{^\prime}}
\newcommand*\arcsec{\ensuremath{^{\prime\prime}}}

\title[SBS\,0335-052E]{Possible Channels for Lyman Continuum Escape in
  the Halo of SBS 0335-52E revealed with VLT/MUSE}

\author{E.~C.~Herenz$^1$,  M.~Hayes$^1$,  P.~Papaderos$^2$, J.~M.~Cannon$^3$, A.~Bik$^1$,
  J.~Melinder$^1$, and G.~Östlin$^1$}

\institute[SU]{$^1$Department of Astronomy, Stockholm University, AlbaNova
  University Centre, SE-106 91, Stockholm, Sweden \\ $^2$Instituto de Astrof\'{i}sica e Ci\^{e}ncias do Espaço - Centro de
  Astrof\'isica da Universidade do Porto, Rua das Estrelas, 4150-762
  Porto, Portugal \\
$^3$Department of Physics \&
  Astronomy, Macalester College, 1600 Grand Avenue, Saint Paul, MN
  55105, USA}

\date{Lyman labyrinths, 11-14 Sep 2019, Kolymbari}

\begin{document}

\begin{frame}
  \begin{columns} 
    \begin{column}{0.99\textwidth} \vspace{+0.8em}
      \begin{beamercolorbox}[center,wd=\textwidth]{postercolumn}
        \begin{block}{SBS\,0335-052: A unique laboratory for physical
            processes at high-$z$}
          SBS\,0335-052, at a redshift of $z=0.0135$, consists of a pair
          of extremely metal-deficient star-forming dwarf galaxies
          separated by 22\,kpc (projected). The system has long
          been recognised as a special laboratory for studies of the
          relevant physical processes in the early universe (Izotov et
          al. 1990, Papaderos et al. 1998).
        \end{block}
    \end{beamercolorbox}
    \end{column}
  \end{columns}
  \begin{columns}[c]
    \begin{column}{0.49\textwidth}
      \begin{beamercolorbox}[center,wd=\textwidth]{postercolumn}
        \begin{block}{Ionised Filaments in the Halo of SBS\,0335-052E}
          We report on the discovery of ionised gas filaments in the
          circum-galactic halo of SBS\,0335-052E in a 1.5\,h
          integration with MUSE.
          \begin{figure}
            \begin{minipage}{\textwidth}
              \centering
              \includegraphics[width=0.9275\textwidth]{figures/Ha_medcontsub_narrowband_1px_smooth_w_scale.pdf}
              \\ \vspace{-0.9em}
              \includegraphics[width=0.9275\textwidth]{figures/OIII_medcontsub_narrowband_1px_smooth_w_scale.pdf}
              \caption{H$\alpha$ (\emph{top}) and [\ion{O}{lll}]
                $\lambda5007$ (\emph{bottom}) narrow band images of
                SBS\,0335-052E created from the MUSE datacube.  East
                is left and North is up.  Contours are drawn at
                $[2.5,5,12.5,1250]\times
                10^{-18}$\,erg\,s$^{-1}$cm$^{-2}$arcsec$^{-2}$ for
                H$\alpha$ and
                $[0.5,5,12.5,1000]\times
                10^{-18}$\,erg\,s$^{-1}$cm$^{-2}$arcsec$^{-2}$ for
                [\ion{O}{lll}]. To highlight the low-SB features the
                images have been smoothed with a $\sigma=1$\,px
                (0.2\arcsec{}) Gaussian. }
              \label{fig:narrow}
            \end{minipage}
          \end{figure}
        \end{block}
        \begin{block}{Filaments in velocity space}
          By fitting a 1D Gaussian to the H$\alpha$ line in each
          spaxel we obtained the velocity field shown in
          Fig.~\ref{fig:vfield}.  Our newly detected filaments connect
          seamlessly in velocity space to the central velocity field.
          \begin{figure}
            \begin{minipage}{\textwidth}
              \centering
              \includegraphics[width=0.8\textwidth]{figures/Ha_velocity_field.pdf}
              \caption{H$\alpha$ line of sight velocity field with H$\alpha$ SB
                contours from Fig.~\ref{fig:narrow}. We spatially smoothed the
                datacube with a tophat filter ($r=1$\arcsec{}) to enhance the
                signal to noise for the fit.}
              \label{fig:vfield}
            \end{minipage}
          \end{figure}
        \end{block}
      \end{beamercolorbox}
    \end{column}
    \begin{column}{0.49\textwidth}
      \begin{beamercolorbox}[center,wd=\textwidth]{postercolumn}
        \begin{block}{The puzzling extended \ion{He}{ll} $\lambda$4686 emission in SBS\,0335-052E}
          SBS\,0335-052E also exhibits significantly extended
          \ion{He}{ll} $\lambda$4686 emission (Fig.~\ref{fig:heii}).
          Recently, Kehrig et al. (2018) quantified the
          \ion{He}{ll}-ionising energy budget of the galaxy using the
          MUSE observations.  This
          study argues against significant contributions from X-Ray
          sources and shocks.  The identified Wolf-Rayet stars also do
          not explain the required amount of $E_\gamma > 4$ Ry photons,
          leading Kehrig et al. (2018) to favour hot massive stars as
          the main agent of \ion{He}{ii}-ionisation.  However, the used
          state-of-the-art BPASS models only reproduce the
          observations at significantly lower-metallicities
          ($Z/Z_\odot \sim 0.05$\%) than exhibited by the
          \ion{H}{ll}-regions ($Z/Z_\odot \sim 3 - 5$\%,
          e.g. Papaderos et al. 2006). %\vspace{0.2em}
          \begin{figure}
            \begin{minipage}{\textwidth}
              \centering
              \includegraphics[width=0.95\textwidth]{figures/HeII_constub_winset_wscale.pdf} %\vspace{1em}
              \caption{\ion{He}{ll} narrow band image from MUSE
                magnified on the central region.  The yellow contour
                is the brightest contour from the H$\alpha$ image
                (Fig.~\ref{fig:narrow}, top). Contours correspond to
                $[2.5,100,1000]\times
                10^{-18}$\,erg\,s$^{-1}$cm$^{-2}$arcsec$^{-2}$. The
                inset in the bottom left shows the emission line free
                HST WFC3/F550M image from the galaxy within the region
                indicated by the red square.}
              \label{fig:heii}
            \end{minipage}
          \end{figure}
        \end{block} \vspace{1mm}
        \begin{block}{\ion{H}{ll} filaments: Channels for Lyman continuum escape?}
          21cm observations reveal that the SBS\,0335 system is
          surrounded by a large HI complex (Pustilnik et al. 2001).
          In Herenz et al. (2017) we conjectured that the reduced
          neutral fraction within the ionised filaments could advocate
          an escape of LyC photons from the starburst into the
          intergalactic medium.  Indeed, newly obtained VLA-B
          configuration data hints at decreased neutral columns
          co-spatially aligned with two filaments
          (Fig.~\ref{fig:final}).  However, in order to trace the full
          extend of the filaments additional MUSE observations are
          required.  In the future we aim at a combined HI-HII study of
          unprecedented depth and quality in the circum-galactic
          environment of this intriguing galaxy.
          \vspace{1em}
          \begin{figure}
            \begin{minipage}{\textwidth}
              \centering
              \includegraphics[width=0.9\textwidth]{figures/lazy_from_prop.pdf} \vspace{1em}
              \caption{Newly obtained VLA HI 21cm observations, first
                reduction of 3h from the total 30h dataset.  Lowest
                contour corresponds to
                $N_\mathrm{H}=8\times10^{19}$cm$^{-2}$.  Black square
                corresponds to our original MUSE pointing, and the
                arrows indicate the position and direction of the
                \ion{H}{ll} filaments.  Further MUSE observations
                would be desirable to trace their full extend.  }
              \label{fig:final}
            \end{minipage}
          \end{figure}
        \end{block} %\vspace{1em}
        \begin{block}{References}
          \textbf{Herenz, E.~C. et al. 2017, A\&A, 606, L11} $\bullet$ Izotov, Y.~I. et
          al. 1990, Nature, 343, 238 $\bullet$ Kehrig, C. et al. 2018,
          MNRAS, 480, 1081 $\bullet$ Papaderos, P. et al.,
          A\&A, 338, 43 $\bullet$ Pustilnik, S.~A. et al. 2001, AJ,
          121, 1413
        \end{block}
      \end{beamercolorbox}
    \end{column}
  \end{columns}
\end{frame}


\end{document}


%%% Local Variables:
%%% mode: latex
%%% TeX-master: t
%%% End:

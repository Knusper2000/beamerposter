\documentclass{beamer}

\usepackage[orientation=portrait,size=a0,scale=1.4]{beamerposter}
\mode<presentation>{\usetheme{ESO}}
\usepackage[utf8]{inputenc}
\usepackage{ragged2e}
\usepackage[font=small,justification=justified]{caption}
\usepackage{array,booktabs,tabularx}
\usepackage{grffile}

\DeclareRobustCommand{\ion}[2]{\textup{#1\,\textsc{\textup{#2}}}}
\newcommand*\arcmin{\ensuremath{^\prime}}
\newcommand*\arcsec{\ensuremath{^{\prime\prime}}}
\DeclareMathOperator\erf{erf}

\title[LSDCat 2.0]{Towards \texttt{LSDCat2.0}: An improved algorithm for detecting
emission line sources in integral-field spectroscopic datasets}

\author{Edmund~Christian~Herenz$^1$}

\institute[]{$^1$ESO Fellow, Santiago, Chile }

\date{Joint Observatories Kavli Science Forum, Santiago (Chile), 2022}

\begin{document}

\begin{frame}
  \begin{columns}[t]
    \begin{column}{0.99\textwidth} %\vspace{+0.3em}
      \begin{block}{Abstract}
        \texttt{LSDCat} is a software (\texttt{ascl:1612.002}) for detecting emission-line sources in 3D integral-field
        spectroscopic (IFS) data [1].  It has been used effectively on MUSE and KCWI cubes and it is an essential part
        of the MUSE-Wide survey data processing pipeline [2,3].  Its algorithm is based upon a slightly simplified
        matched-filter ansatz.  It provides optimal detection significances under the assumption of stationary noise.  However,
        the high frequency and high amplitude noise background from the "forest" of telluric OH Meinel bands at longer
        wavelengths invalidate this assumption.  Thus detection significances for emission-line sources falling on or in
        between sky-lines are biased low.  We here present an improved algorithm that remedies this shortcoming.  The
        improvement will be part of an updated \texttt{LSDCat2.0}.  It will benefit especially the detection of
        $z \gtrsim 5$ Lyman-$\alpha$ emitters in the deepest MUSE data, e.g., in the $t_\mathrm{exp} = 140$\,h MUSE
        eXtreme deep field.
        \end{block}
    \end{column}
  \end{columns}
  \vspace{-0.5cm}
  \begin{columns}[t]
    \begin{column}{0.48\textwidth}
      \begin{block}{LSDCat --  Line Source Detection and Cataloguing Tool}
        \texttt{LSDCat} cross-correlates IFS datacubes $f$ with a template $s$ and propagates
        the variances  $\sigma^2$ accordingly.  In 1D these operations can be expressed as
        $\tilde{f}_i = \sum_{j=-k}^{k} s_j f_{i-j}$ and $\tilde{\sigma}_i^2 = \sum_{j=-k}^k s_j^{2} \sigma_{i-j}^2$.  Then
        the signal-to-noise ratio
            \begin{equation}
              \label{eq:1}
              {SN}^{\mathrm{LSD}}_i =
              \frac{\tilde{f}_i}{\tilde{\sigma}_i} =
              \frac{\sum_{j=-k}^k s_j f_{j-i}
              }{\sqrt{\sum_{j=-k}^k s_j^{2} \sigma_{i-j}^2 } }
            \end{equation}
            is maximised if the template matches the sources.
            In Fig.~\ref{fig:1} the effect of Eq.~(\ref{eq:1}) on a Lyman-$\alpha$ emitter in a MUSE datacube is
            demonstrated.  After this operation \texttt{LSDCat} collects emission line sources into a catalogue via
            thresholding  in ${SN}^{\mathrm{LSD}}$.
            \begin{figure}
              \begin{minipage}{\textwidth}
                \centering 
                \includegraphics[width=\textwidth,trim=0 290 0 0,clip=true]{figures/detsn_example_v2.pdf}
                \caption{Example of how LSDCat filtering converts the MUSE datacube into a $\mathrm{S/N}$ cube from
                  which a catalogue of sources can be constructed via simple thresholding [1].  The panels in the first
                  row display four different spectral layers of the continuum-subtracted datacube.  The leftmost panels
                  show a layer significantly away from the emission line peak, the other panels show layers at spectral
                  coordinates $z_{\mathrm{peak}}-1$, $z_{\mathrm{peak}}$, and $z_{\mathrm{peak}}+1$, respectively, where
                  $z_{\mathrm{peak}}$ designates the layer containing the maximum $S/N$ value of the source.  The source
                  shown is a Lyman $\alpha$ emitting galaxy at redshift $z=3.28$ with flux
                  $F_\mathrm{Ly\alpha} = 1.3\times10^{-18}$\,erg\,s$^{-1}$cm$^{-2}$ in the MUSE Hubble Deep Field South
                  data [4].}
                \label{fig:1}
              \end{minipage}
            \end{figure}
          \end{block}
          \vspace{0.6cm}
          \begin{block}{Eq.~(\ref{eq:1}) does not provide maximum S/N if $\sigma_i^2$ varies strongly with $i$}
            At $\lambda \gtrsim 6000$\,\AA{} the variances $\sigma_i^2$ vary strongly as function of wavelength due to
            the sky-lines (OH Meinel bands).  We can incorporate $\sigma^2_i$ into a filter $T$ that we cross-correlate
            with the data $f$, i.e. $\tilde{f}_i = \sum_{j=-k}^{k} T^{(i)}_j f_{i-j}$ with
            \begin{equation}
              \label{eq:2}
              T^{(i)}_j = \frac{1}{\sqrt{\sum_{j=-k}^{k}
                  \frac{s_j^2}{\sigma_{i-j}^2}}} \times \frac{s_j}{\sigma_{i-j}^2} \; \text{.}
            \end{equation}
            This is the formal definition of a matched filter for uncorrelated variances and 
            \begin{equation}
              \label{eq:4}
              SN^\mathrm{MF}_i \equiv \tilde{f}_i  = \sum_{j=-k}^{k} T^{(i)}_j f_{i-j} = \frac{\sum_{j=-k}^{k}
                \frac{s_j}{\sigma_{i-j}^2} f_{i-j} }{\sqrt { \sum_{j=-k}^{k}
                  \frac{s_j^2}{\sigma_{i-j}^2 }}} \; \text{}
            \end{equation}
            is the maximum S/N that is achievable if $s$ matches the source (e.g., [5]).

            \vspace{0.3em}
            We note that Eq.~(\ref{eq:1}) with $\sigma_i^{2} \approx \sigma^2_{\mathrm{const.}}$ results in
            \begin{equation}
              \label{eq:5}
              SN^\mathrm{LSD}_i \approx \frac{\sum_{j=-k}^k s_j f_{j-i}
              }{\sqrt{\sum_{j=-k}^k s_j^{2} \sigma_\mathrm{const}^2 } } =
              \frac{\sum_{j=-k}^k s_j f_{j-i} }{\sigma_\mathrm{const}
                \sqrt{\sum_{j=-k}^k s_j^{2} } } \; \text{,} \notag
            \end{equation}
            and that Eq.~(\ref{eq:4}) under the same provision becomes
            \begin{equation}
              \label{eq:6}
              SN^\mathrm{MF}_i \approx
              \frac{\frac{1}{\sigma_\mathrm{const}^2} \sum_{j=-k}^{k} s_j f_{i-j}
              }{\frac{1}{\sigma_\mathrm{const} }\sqrt{\sum_{j=-k}^{k} s_j^2}} =
              \frac{\sum_{j=-k}^{k} s_j f_{i-j}}{\sigma_\mathrm{const}
                \sqrt{\sum_{j=-k}^{k} s_j^2}} \;\text{.} \notag
            \end{equation}
            Thus Eq.~(\ref{eq:4})~$\cong$~Eq.~(\ref{eq:1}) if $\sigma_i^2$ does not vary strongly with $i$; i.e., the
            current algorithm of LSDCat implements a matched filter for stationary and uncorrelated noise.  It is thus
            known that the $S/N$ values for emission lines in the red part of the MUSE datacube can be biased low
            (see Figure~\ref{fig:2}).  For applications in the deepest MUSE datasets it has become desirable to remedy
            this situation.
          \end{block}
    \end{column}
    \begin{column}{0.48\textwidth}
      \begin{block}{Demonstration: Eq.~(\ref{eq:1}) [\texttt{LSDCat}] vs. Eq.~(\ref{eq:4}) [\texttt{LSDCat2.0}]}
        In Fig.~\ref{fig:2} we demonstrate the SN improvement by using Eq.~(\ref{eq:4}) over Eq.~(\ref{eq:1}) for an
        emission line signal affected by varying variances.
        \begin{figure}
          \begin{minipage}{\textwidth}
            \centering
            \includegraphics[width=\textwidth,trim=40 45 40 45,clip=true]{figures/var_noise.png}
            \caption{The \textit{top left panel} shows the set-up for the experiment: an emission line that is affected
              by increased variances in both wings of the line.  The \textit{top right} panel shows one random
              realisation of this set-up.  The \emph{bottom left panel} or \emph{bottom right panel} shows the resulting
              SN from Eq.~(\ref{eq:1}) or Eq.~(\ref{eq:4}), respectively.  In both bottom panels the result for the
              random realisation in the top right panel is shown as a green line; the mean and the 10- to 90-percentile
              interval from $1000$ MC realisations  are shown as a blue line and a blue-shaded region,
              respectively.}
            \label{fig:2}
          \end{minipage}
        \end{figure}
      \end{block}
      \begin{block}{Modifications for \texttt{LSDCat2.0}}
        \texttt{LSDCat} implements a 3D filter, $s_{i,j,k}$, in Eq.~(\ref{eq:1}) that is separated into a 2D
        ($s_{i,j}^\mathrm{spat}$, e.g., the point-spread function) and a 1D ($s_k^\mathrm{spec}$, usually Gaussian
        profiles) part: $s_{i,j,k} = s_{i,j}^\mathrm{spat} s_k^\mathrm{spec}$.  We can rewrite Eq.~(\ref{eq:2})
        similarly as $T^{(x,y,z)}_{i,j,k} = T_k^{(z)} T_{ij}^{(z)}$ asserting that $\sigma^2_{x,y,z} \approx \sigma^2_z$
        (i.e. variances are independent of position):
        \begin{equation}
          \label{eq:7}
            T_k^{(z)} = \frac{1}{\sqrt{\sum\limits_{n}
            \frac{(s_n^{(z)})^2}{\sigma^2_{z-n}}}} \times \frac{s^{(z)}_k}{\sigma_{z-k}^2}
        \quad \text{and} \quad
          T_{ij}^{(z)} = \frac{s_{ij}^{(z)}}{\sqrt{\sum\limits_{lm}
               (s_{lm}^{(z)} )^2}}
        \end{equation}
        and Eq.~(\ref{eq:4}) becomes
        \begin{equation}
          \label{eq:8}
          {SN}^\mathrm{MF}_{x,y,z} = \tilde{F}_{x,y,z} = \sum_k T_k^{(z)} \sum_{i,j} T_{ij}^{(z)} F_{x-i,y-j,z-k}  \;\text{.}
        \end{equation}
        Eq.~(\ref{eq:7}) describes the necessary modifications to the current filter design.  An average 1D variance
        spectrum needs to be created from the variance cubes prior to this operation.  \texttt{LSDCat} computes already
        Eq.~(\ref{eq:8}) effectively and embarrisingly paralellel; it uses FFT for $s_{i,j}^\mathrm{spat}$ and
        sparse-matrix multiplication for $s_k^\mathrm{spec}$.
      \end{block}
      \begin{block}{Predicting $SN_\mathrm{MF}$ for given emission line flux $F_\mathrm{line}$}
        Assuming an optimal template match for a source at $(x',y',z')$,
        $F_{x,y,z} = \frac{F_\mathrm{line}}{\Delta \lambda} s_{x-x',y-y',z-z'} = \frac{F_\mathrm{line}}{\Delta \lambda}
        s_{i,j}^\mathrm{spat} s_k^\mathrm{spec}$, we have from Eq.~(\ref{eq:7}) and Eq.~(\ref{eq:8})
        \begin{equation}
          \label{eq:9}
          {SN}^\mathrm{MF}_{x',y',z'} 
          = \frac{F_\mathrm{line}}{\Delta \lambda}
            \sqrt{\sum\limits_{ij} \left(s_{ij}^\mathrm{spat} \right)^2 } \times
      \sqrt{\sum\limits_k \left ( \frac{s_k^\mathrm{spec}}{
            \sigma_{z'-k} } 
            \right )^2} %  \\
        \end{equation}
        Eq.~(\ref{eq:9}) can be evaluated numerically.  The selection function for sources above a given detection
        threshold $SN_\mathrm{det}$ in the IFS datacube can then be expressed as
        \begin{equation}
          \label{eq:10}
          f_C(F_\mathrm{line},\lambda | SN_\mathrm{def}) = \frac{1}{2} \left [ 1 + \erf \left (
              \frac{SN^\mathrm{MF} \left (F_\mathrm{line},z(\lambda) \right ) - SN_\mathrm{det}}{\sqrt{2}}  \right )\right ]
        \end{equation}
        where $\erf(x) = \frac{2}{\sqrt{\pi}} \int_0^x \mathrm{e}^{-t^2} \,\mathrm{d}t$.  Eq.~(\ref{eq:9}) and Eq.~(\ref{eq:10}) are essential,
        e.g., for estimating the Lyman-$\alpha$ emitter luminosity function from MUSE surveys [6,7].
      \end{block}%\vspace{-0.cm}
    \end{column}
  \end{columns}
  \begin{columns}
    \begin{column}{0.99\textwidth} \vspace{-0.6cm}
      \begin{block}{\small References}
        \footnotesize [1] Herenz E.~C., Wisotzki L., 2017, A\&A, 602, A111 -- [2] Urrutia, T., Wisotzki, L., Kerutt, J.,
        et al.\ 2019, A\&A, 624, A141 -- [3] Herenz, E.~C., Urrutia, T., Wisotzki, L., et al.\ 2017, A\&A, 606, A12 --
        [4] Bacon, R., Brinchmann, J., Richard, J., et al.\ 2015, A\&A, 575, A75 -- [5]  Vio, R. \& Andreani, P.\ 2021,
        arXiv:2107.09378 -- [6] Herenz, E.~C., Wisotzki, L., Saust, R., et al.\ 2019, A\&A, 621, A107 -- [7] Herenz et al., in prep.
      \end{block}
    \end{column}
  \end{columns}
\end{frame}


\end{document}


%%% Local Variables:
%%% mode: latex
%%% TeX-master: t
%%% End:
